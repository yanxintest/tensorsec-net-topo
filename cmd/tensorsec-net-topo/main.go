package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/yanxintest/tensorsec-net-topo/api/nettopo"

	"github.com/sirupsen/logrus"
)

var sysSignals = []os.Signal{
	syscall.SIGTERM,
}

func main() {
	apiServer, err := nettopo.New()
	if err != nil {
		logrus.Error(err, "failed to init API server")
		os.Exit(1)
	}

	errCh := make(chan error, 1)
	apiServer.Launch(errCh)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, sysSignals...)

	select {
	case err = <-errCh:
		logrus.Error(err, "failed to launch API server")
	case <-sc:
		cancel()
		if err := apiServer.Shutdown(ctx); err != nil {
			logrus.Error(err, "failed to shut down API server")
		}
	}
}
