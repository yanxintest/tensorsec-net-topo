# app version
VERSION = v$(shell cat version/version)

WORKSPACE ?= tensorsec-net-topo

PKG = $(shell cat go.mod | grep "^module " | sed -e "s/module //g")
NAME = $(shell basename $(PKG))
COMMIT_SHA ?= $(shell git rev-parse --short HEAD)
APPVERSION_KEY := $(PKG)/version.AppVersion
GITVERSION_KEY := $(PKG)/version.GitRevision
LDFLAGS ?= "-X $(APPVERSION_KEY)=$(VERSION) -X $(GITVERSION_KEY)=$(COMMIT_SHA)"

GOOS ?= $(shell go env GOOS)
GOARCH ?= $(shell go env GOARCH)
GO = GO111MODULE=on CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) go

GOBUILD=$(GO) build -a -ldflags $(LDFLAGS)

download:
	go mod download

up: imports fmt download openapi
	$(GO) run ./cmd/$(WORKSPACE)

gen-config:
	go run hack/genconfig/generate.go

build:
	$(GOBUILD) -o ./bin/$(WORKSPACE)-$(GOOS)-$(GOARCH) ./cmd/$(WORKSPACE)

openapi:
	go get -u github.com/swaggo/swag/cmd/swag
	swag init -g api/nettopo/route.go --output docs

lint:
	golangci-lint run -v

fmt:
	go fmt ./...

imports:
	goimports -w .