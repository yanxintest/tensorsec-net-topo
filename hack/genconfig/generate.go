package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"

	"gitlab.com/yanxintest/tensorsec-net-topo/config"

	"github.com/sirupsen/logrus"
)

func main() {
	fileName := "config/config_template.yaml"
	f, _ := os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC, os.ModePerm)
	defer f.Close()
	w := bufio.NewWriter(f)
	t := reflect.TypeOf(config.Config{})
	err := genConfigTemplate(w, t, 0)
	if err != nil {
		logrus.Error(err, "failed to create config template")
		return
	}
	err = w.Flush()
	if err != nil {
		logrus.Error(err, "failed to create config template")
	}
}

func genConfigTemplate(w *bufio.Writer, t reflect.Type, h int) error {
	indentStr := ""
	for j := 0; j < h; j++ {
		indentStr += "  "
	}
	for i := 0; i < t.NumField(); i++ {
		_, err := w.WriteString(fmt.Sprintf("%s%s:\n", indentStr, t.Field(i).Tag.Get("mapstructure")))
		if err != nil {
			return err
		}
		if t.Field(i).Type.Kind() == reflect.Struct {
			err = genConfigTemplate(w, t.Field(i).Type, h+1)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
