package util

const (
	// RootPath indicates url root path
	RootPath = "/api"
	// APIVersion indicates version of api
	APIVersion = "v2"
	// ModuleName indicates module name
	ModuleName = "platform"
)
