package util

import (
	"net/http"

	"gitlab.com/yanxintest/tensorsec-net-topo/api"

	"github.com/gin-gonic/gin"
)

func AssembleResponse(c *gin.Context, data interface{}, err error) {
	code := http.StatusOK
	if err != nil {
		code = http.StatusInternalServerError
		c.JSON(code, api.FailResponse{
			APIVersion: APIVersion,
			Error: api.RespErr{
				Code:    code,
				Message: err.Error(),
			},
		})
		return
	}

	c.JSON(code, api.SuccessResponse{
		APIVersion: APIVersion,
		Data:       data,
	})
}
