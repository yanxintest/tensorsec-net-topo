package nettopo

import (
	"fmt"

	"gitlab.com/yanxintest/tensorsec-net-topo/pkg/model/nettopo"
)

var _ Service = (*netTopo)(nil)

func (n *netTopo) ListUpstreamInfo(scluster, sns, skind, sname string, qw int) ([]string, int, error) {
	res := []string{}
	m := nettopo.NewNetTopoModel(n.db)
	upinfo, err := m.ListUpstreamInfo(scluster, sns, skind, sname, qw)
	if err != nil {
		return nil, 0, err
	}
	for _, u := range upinfo {
		res = append(res, fmt.Sprintf("%s:%s:%s:%s", u.DstCluster, u.DstNamespace, u.DstKind, u.DstName))
	}
	return res, len(upinfo), nil
}

func (n *netTopo) ListDownstreamInfo(dcluster, dns, dkind, dname string, qw int) ([]string, int, error) {
	res := []string{}
	m := nettopo.NewNetTopoModel(n.db)
	downinfo, err := m.ListDownstreamInfo(dcluster, dns, dkind, dname, qw)
	if err != nil {
		return nil, 0, err
	}
	for _, u := range downinfo {
		res = append(res, fmt.Sprintf("%s:%s:%s:%s", u.SrcCluster, u.SrcCluster, u.SrcCluster, u.SrcCluster))
	}
	return res, len(downinfo), nil
}
