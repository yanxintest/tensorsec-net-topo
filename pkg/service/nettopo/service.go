package nettopo

type Service interface {
	ListUpstreamInfo(scluster, sns, skind, sname string, qw int) ([]string, int, error)
	ListDownstreamInfo(dcluster, dns, dkind, dname string, qw int) ([]string, int, error)
}
