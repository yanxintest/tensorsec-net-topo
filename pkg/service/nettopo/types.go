package nettopo

import "gorm.io/gorm"

type netTopo struct {
	db *gorm.DB
}

func NewNetTopoService(db *gorm.DB) Service {
	return &netTopo{
		db: db,
	}
}
