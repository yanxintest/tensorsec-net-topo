package nettopo

import "time"

type Model interface {
	ListUpstreamInfo(scluster, sns, skind, sname string, qw int) ([]*TensorNetworkFlows, error)
	ListDownstreamInfo(dcluster, dns, dkind, dname string, qw int) ([]*TensorNetworkFlows, error)
}

type TensorNetworkFlows struct {
	UUID         int64
	SrcCluster   string `gorm:"type:varchar(100)"`
	SrcNamespace string `gorm:"type:varchar(100)"`
	SrcKind      string `gorm:"type:varchar(100)"`
	SrcName      string `gorm:"type:varchar(100)"`
	DstCluster   string `gorm:"type:varchar(100)"`
	DstNamespace string `gorm:"type:varchar(100)"`
	DstKind      string `gorm:"type:varchar(100)"`
	DstName      string `gorm:"type:varchar(100)"`
	Status       int
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func (t *TensorNetworkFlows) TableName() string {
	return "tensor_network_flows"
}
