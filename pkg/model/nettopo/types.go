package nettopo

import (
	"gorm.io/gorm"
)

type netTopo struct {
	db *gorm.DB
}

func NewNetTopoModel(db *gorm.DB) Model {
	return &netTopo{
		db: db,
	}
}
