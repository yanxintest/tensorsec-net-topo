package nettopo

import (
	"time"
)

var _ Model = (*netTopo)(nil)

func (n *netTopo) ListUpstreamInfo(scluster, sns, skind, sname string, qw int) ([]*TensorNetworkFlows, error) {
	var tfs []*TensorNetworkFlows
	err := n.db.Select("dst_cluster,dst_namespace,dst_kind,dst_name").
		Where("updated_at > ?", time.Now().Add(-time.Duration(qw)*time.Hour)).
		Where("src_cluster = ?", scluster).
		Where("src_namespace = ?", sns).
		Where("src_kind = ?", skind).
		Where("src_name = ?", sname).
		Find(&tfs).Error
	if err != nil {
		return nil, err
	}
	return tfs, nil
}

func (n *netTopo) ListDownstreamInfo(dcluster, dns, dkind, dname string, qw int) ([]*TensorNetworkFlows, error) {
	var tfs []*TensorNetworkFlows
	err := n.db.Select("src_cluster,src_namespace,src_kind,src_name").
		Where("updated_at > ?", time.Now().Add(-time.Duration(qw)*time.Hour)).
		Where("dst_cluster = ?", dcluster).
		Where("dst_namespace = ?", dns).
		Where("dst_kind = ?", dkind).
		Where("dst_name = ?", dname).
		Find(&tfs).Error
	if err != nil {
		return nil, err
	}
	return tfs, nil
}
