package version

// GitRevision is the commit of repo
var GitRevision = "UNKNOWN"

// AppVersion is the version of application
var AppVersion = "UNKNOWN"
