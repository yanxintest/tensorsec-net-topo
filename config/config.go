package config

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Config struct {
	Address  string `mapstructure:"address"` // address for apiserver
	RunMode  string `mapstructure:"runMode"` // dev or prod
	PgMaster struct {
		Host     string `mapstructure:"host"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
		DBName   string `mapstructure:"dbName"`
		Port     int    `mapstructure:"port"`
	} `mapstructure:"pgMaster"`
	QueryWindow int `mapstructure:"queryWindow"`
}

func InitConfig() (*Config, error) {
	cfg := &Config{}
	viper.AutomaticEnv()
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, errors.Wrap(err, "failed to read config")
	}
	err = viper.Unmarshal(cfg)
	if err != nil {
		return nil, errors.Wrap(err, "unable to decode into struct")
	}

	if cfg.RunMode == "dev" {
		logrus.SetReportCaller(true)
		logrus.SetLevel(logrus.DebugLevel)
	}

	logrus.Infof("init application with config: %+v", cfg)
	return cfg, nil
}
