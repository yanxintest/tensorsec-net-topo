# build
FROM golang:1.15.7-alpine3.13 AS builder

# setup private pkg
ARG GOPROXY=https://goproxy.cn,direct
ARG GOPRIVATE=gitlab.com/tensorsecurity-rd

RUN apk update
RUN apk add --no-cache git musl-dev gcc
WORKDIR /go/src/app
COPY ./ ./
# install dep
RUN go mod download
# openapi
RUN make openapi
# build
RUN make build

# runtime
FROM scratch

COPY --from=builder /go/src/app/bin/* /app/tensorsec-net-topo
COPY --from=builder /go/src/app/config /app/
EXPOSE 80 8080

WORKDIR /app
ENTRYPOINT ["./tensorsec-net-topo"]
