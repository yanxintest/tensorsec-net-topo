package nettopo

import (
	"fmt"
	"net/http"
	"os"

	// swagger json data
	_ "gitlab.com/yanxintest/tensorsec-net-topo/docs"
	"gitlab.com/yanxintest/tensorsec-net-topo/pkg/util"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title tensorsec-net-topo Restful API
// @description An API.
// @version 0.0.0
// @description network topology map
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host 127.0.0.1:8888
// @BasePath /api/v2/platform
func (s *APIServer) setupRoute() http.Handler {
	router := gin.New()
	loggerConfig := gin.LoggerConfig{
		Output: os.Stdout,
		Formatter: func(param gin.LogFormatterParams) string {
			return fmt.Sprintf("%v | %3d | %13v | %15s | %-7s %s | %s\n",
				param.TimeStamp.Format("2006/01/02 - 15:04:05"),
				param.StatusCode,
				param.Latency,
				param.ClientIP,
				param.Method,
				param.Path,
				param.ErrorMessage,
			)
		},
	}

	router.Use(gin.LoggerWithConfig(loggerConfig))
	// router.Use(util.SetRequestID())
	// router.Use(util.SetContext())
	router.Use(gin.Recovery())
	// router.Use(util.ValidateHeaders())
	api := router.Group(util.RootPath).Group("/" + util.APIVersion).Group("/" + util.ModuleName)
	mapinfo := api.Group("/networkTopo")
	{
		mapinfo.GET("/upstream/cluster/:cluster/namespace/:namespace/kind/:kind/resource/:resource", s.ListUpstreamInfo)
		mapinfo.GET("/downstream/cluster/:cluster/namespace/:namespace/kind/:kind/resource/:resource", s.ListDownstreamInfo)
	}
	swaggers := router.Group("/swagger")
	{
		url := ginSwagger.URL("/swagger/swagger.json")
		swaggers.GET("/*any", s.SwaggerJSON, ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	}
	return router
}
