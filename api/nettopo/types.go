package nettopo

type ListRespData struct {
	Items        []Resource `json:"items"`
	ItemsPerPage int        `json:"itemsPerPage"`
	TotalItems   int        `json:"totalItems"`
}

type Resource = string
