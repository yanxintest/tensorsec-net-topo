package nettopo

import (
	"gitlab.com/yanxintest/tensorsec-net-topo/pkg/service/nettopo"
	"gitlab.com/yanxintest/tensorsec-net-topo/pkg/util"

	"github.com/gin-gonic/gin"
)

// ListUpstreamInfo lists upstreamInfo
// @Tags networkTopo
// @ID listUpstreamInfo
// @Param cluster path string true "cluster the resource belong to"
// @Param namespace path string true "namespace the resource belong to"
// @Param kind path string true "resource kind"
// @Param resource path string true "resource name"
// @Success 200 {object} api.SuccessResponse{apiVersion=string,data=ListRespData}
// @Failure 500 {object} api.FailResponse{apiVersion=string,error=api.RespErr}
// @Router /networkTopo/upstream/cluster/{cluster}/namespace/{namespace}/kind/{kind}/resource/{resource} [get]
func (s *APIServer) ListUpstreamInfo(c *gin.Context) {
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	kind := c.Param("kind")
	resource := c.Param("resource")
	var resp *ListRespData
	netTopo := nettopo.NewNetTopoService(s.db)
	data, total, err := netTopo.ListUpstreamInfo(cluster, namespace, kind, resource, s.cfg.QueryWindow)
	if err != nil {
		util.AssembleResponse(c, resp, err)
	}
	resp = &ListRespData{
		Items:        data,
		ItemsPerPage: 0,
		TotalItems:   total,
	}
	util.AssembleResponse(c, resp, err)
}

// ListDownstreamInfo lists downstreamInfo
// @Tags networkTopo
// @ID listDownstreamInfo
// @Param cluster path string true "cluster the resource belong to"
// @Param namespace path string true "namespace the resource belong to"
// @Param kind path string true "resource kind"
// @Param resource path string true "resource name"
// @Success 200 {object} api.SuccessResponse{apiVersion=string,data=ListRespData}
// @Failure 500 {object} api.FailResponse{apiVersion=string,error=api.RespErr}
// @Router /networkTopo/downstream/cluster/{cluster}/namespace/{namespace}/kind/{kind}/resource/{resource} [get]
func (s *APIServer) ListDownstreamInfo(c *gin.Context) {
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	kind := c.Param("kind")
	resource := c.Param("resource")
	var resp *ListRespData
	netTopo := nettopo.NewNetTopoService(s.db)
	data, total, err := netTopo.ListDownstreamInfo(cluster, namespace, kind, resource, s.cfg.QueryWindow)
	if err != nil {
		util.AssembleResponse(c, resp, err)
	}
	resp = &ListRespData{
		Items:        data,
		ItemsPerPage: 0,
		TotalItems:   total,
	}
	util.AssembleResponse(c, resp, err)
}
