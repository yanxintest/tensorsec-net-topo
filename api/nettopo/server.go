package nettopo

import (
	"context"
	goerr "errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/yanxintest/tensorsec-net-topo/config"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type APIServer struct {
	server *http.Server
	db     *gorm.DB
	cfg    *config.Config
}

func New(opts ...Option) (*APIServer, error) {
	cfg, err := config.InitConfig()
	if err != nil {
		return nil, errors.Wrap(err, "failed to init config")
	}
	db, err := initDB(cfg)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init db")
	}
	s := &APIServer{
		db:  db,
		cfg: cfg,
	}
	for _, opt := range opts {
		opt(s)
	}
	server := &http.Server{
		Addr:         cfg.Address,
		Handler:      s.setupRoute(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	server.SetKeepAlivesEnabled(true)
	s.server = server
	return s, nil
}

func initDB(cfg *config.Config) (*gorm.DB, error) {
	dbcfg := cfg.PgMaster
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Shanghai",
		dbcfg.Host, dbcfg.User, dbcfg.Password, dbcfg.DBName, dbcfg.Port,
	)
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: dsn,
	}), &gorm.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "failed to connet specified db")
	}
	return db, nil
}

// Launch will start the apiserver
func (s *APIServer) Launch(errChan chan<- error) {
	go func() {
		err := s.server.ListenAndServe()
		if err != nil && goerr.Is(err, http.ErrServerClosed) {
			errChan <- err
		}
	}()
}

// Shutdown will close the apiserver
func (s *APIServer) Shutdown(ctx context.Context) error {
	logrus.Info("sever shutting down")
	return s.server.Shutdown(ctx)
}
