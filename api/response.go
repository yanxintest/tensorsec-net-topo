package api

type SuccessResponse struct {
	APIVersion string      `json:"apiVersion"`
	Data       interface{} `json:"data"`
}

type FailResponse struct {
	APIVersion string  `json:"apiVersion"`
	Error      RespErr `json:"error"`
}

type RespErr struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
